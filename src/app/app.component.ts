import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  passwordMatched:boolean=false;
  title = 'app';
  formSubmit(myForm){
    console.log(myForm);
  }
  confirmPasswrd(pw,cpw){
  if(pw===cpw)  
    this.passwordMatched=true;
  
  else
  this.passwordMatched=false;
  }
}
